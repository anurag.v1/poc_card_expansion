package com.example.poc_card

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView


class MainActivity : AppCompatActivity() {

    private lateinit var card1: CardView
    private lateinit var card2: CardView
    private lateinit var card3: CardView
    private lateinit var desc1: TextView
    private lateinit var desc2: TextView
    private lateinit var desc3: TextView
    private lateinit var sub1: TextView
    private lateinit var sub2: TextView
    private lateinit var sub3: TextView
    private lateinit var anim1: Animation
    private lateinit var anim2: Animation
    private lateinit var img1: ImageView
    private lateinit var img2: ImageView
    private lateinit var img3: ImageView
    private lateinit var imgdown1: ImageView
    private lateinit var imgdown2: ImageView
    private lateinit var imgdown3: ImageView
    private lateinit var txt1: TextView
    private lateinit var txt2: TextView
    private lateinit var txt3: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        card1 = findViewById(R.id.card1)
        card2 = findViewById(R.id.card2)
        card3 = findViewById(R.id.card3)
        desc1 = findViewById(R.id.description1)
        desc2 = findViewById(R.id.description2)
        desc3 = findViewById(R.id.description3)
        sub1 =  findViewById(R.id.subtitle1)
        sub2 =  findViewById(R.id.subtitle2)
        sub3 =  findViewById(R.id.subtitle3)
        img1= findViewById(R.id.image1)
        img2 = findViewById(R.id.image2)
        img3 = findViewById(R.id.image3)
        imgdown1=findViewById(R.id.image_down1)
        imgdown2=findViewById(R.id.image_down2)
        imgdown3=findViewById(R.id.image_down3)
        txt1=findViewById(R.id.text1)
        txt2=findViewById(R.id.text2)
        txt3=findViewById(R.id.text3)

        val isExpanded = booleanArrayOf(false, false, false)


        // Set click listeners for cards
        card1.setOnClickListener {
            expandCard(txt1,card1, desc1, sub1, isExpanded[0],img1,imgdown1)
            if(!isExpanded[0]) {
                collapseCard(txt2, card2, desc2, sub2, img2, imgdown2)
                textRotation(txt2)
                collapseCard(txt3, card3, desc3, sub3, img3, imgdown3)
                textRotation(txt3)
            }
            else{
                collapseCard(txt2, card2, desc2, sub2, img2, imgdown2)
                textBackRotation(txt2)
                collapseCard(txt3, card3, desc3, sub3, img3, imgdown3)
                textBackRotation(txt3)
            }
            isExpanded[0]=!isExpanded[0]
            isExpanded[1]=false
            isExpanded[2]=false
        }

        card2.setOnClickListener {
            expandCard(txt2,card2, desc2, sub2, isExpanded[1],img2,imgdown2)
            if(!isExpanded[1]) {
                collapseCard(txt1,card1, desc1,  sub1,img1,imgdown1)
                textRotation(txt1)
                collapseCard(txt3, card3, desc3, sub3, img3, imgdown3)
                textRotation(txt3)
            }
            else{
                collapseCard(txt1,card1, desc1,  sub1,img1,imgdown1)
                textBackRotation(txt1)
                collapseCard(txt3, card3, desc3, sub3, img3, imgdown3)
                textBackRotation(txt3)
            }
            isExpanded[1]=!isExpanded[1]
            isExpanded[0]=false
            isExpanded[2]=false
        }

        card3.setOnClickListener {
            expandCard(txt3,card3, desc3, sub3, isExpanded[2],img3,imgdown3)
            if(!isExpanded[2]) {
                collapseCard(txt1, card1, desc1, sub1, img1, imgdown1)
                textRotation(txt1)
                collapseCard(txt2, card2, desc2, sub2, img2, imgdown2)
                textRotation(txt2)
            }
            else{
                collapseCard(txt1, card1, desc1, sub1, img1, imgdown1)
                textBackRotation(txt1)
                collapseCard(txt2, card2, desc2, sub2, img2, imgdown2)
                textBackRotation(txt2)
            }
            isExpanded[2]=!isExpanded[2]
            isExpanded[1]=false
            isExpanded[0]=false
        }
    }

    private fun expandCard(title: TextView,cardView: CardView, descView: TextView, subTitle: TextView, isExpanded: Boolean,img: ImageView, imgDown: ImageView) {
        if(!isExpanded) {
            val initialWidth = cardView.width

            cardView.layoutParams = cardView.layoutParams.apply {
                width = resources.getDimensionPixelSize(R.dimen.card_width_expanded)
            }
            descView.visibility = View.VISIBLE
            subTitle.visibility = View.VISIBLE
            val anim3 = ObjectAnimator.ofFloat(subTitle, "translationY", 0f,title.height.toFloat())
            anim3.duration = 200
            anim3.start()
            val anim5 = ObjectAnimator.ofFloat(descView, "translationY", 0f,title.height.toFloat())
            anim5.duration = 200
            anim5.start()
            val anim6 = ObjectAnimator.ofFloat(title, "translationY", -(cardView.height-2*title.height).toFloat())
            anim6.duration = 200
            anim6.start()
            textBackRotation(title)
            imgDown.visibility=View.VISIBLE
            img.visibility=View.GONE
        }
        else{
            collapseCard(title, cardView, descView, subTitle, img, imgDown)
            val anim6 = ObjectAnimator.ofFloat(title, "translationY", 0f)
            anim6.duration = 200
            anim6.start()
        }
    }

    private fun collapseCard(title: TextView,cardView: CardView, descView: TextView, subTitle: TextView,img: ImageView, imgDown: ImageView) {
        cardView.layoutParams = cardView.layoutParams.apply {
            width = resources.getDimensionPixelSize(R.dimen.card_width_collapsed)
        }
        descView.visibility = View.GONE
        subTitle.visibility = View.GONE
        imgDown.visibility=View.GONE
        img.visibility=View.VISIBLE
        val anim6 = ObjectAnimator.ofFloat(title, "translationY", 0f)
        anim6.duration = 20
        anim6.start()
    }
    private fun textRotation(title: TextView){
        val animator = ObjectAnimator.ofFloat(0f, -90f)
        animator.duration = 200
        animator.addUpdateListener {
            title.rotation = it.animatedValue as Float
//            val params: ViewGroup.LayoutParams = title.getLayoutParams()
////            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
//            params.width= 2*title.height
//            title.setLayoutParams(params)
        }
        animator.start()
    }
    private fun textBackRotation(title: TextView){
        val animator = ValueAnimator.ofFloat(-90f,0f)
        animator.duration = 200
        animator.addUpdateListener {
            title.rotation = it.animatedValue as Float
//            val params: ViewGroup.LayoutParams = title.getLayoutParams()
//            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
////            params.height= cardView.height
//            title.setLayoutParams(params)
        }
        animator.start()
    }
}


